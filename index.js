
/**
 * bài 1
 * input:
 *  1 ngày làm 100.000
 *  nhập số ngày làm từ bàn phím
 * bước xử lý
 * b1: tạo biến soNgay, luongNgay, tongLuong
 * b2: lấy giá trị soNgay bằng value
 * b3: lấy giá trị luongNgay bằng value
 * b4: tinh tongLuong = luongNgay * soNgay
 * b5: in kết quả tongLuong bằng innerHTML
 * 
 * output:
 *  kết quả tongLuong (tổng lương)
 */
var result = document.getElementById("result");
function tinhTien(){
    var luongNgay = document.getElementById("luongNgay").value*1;
    var soNgay = document.getElementById("soNgay").value*1;
    var tongLuong = luongNgay * soNgay;
    result.innerHTML = `<h3>${new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(tongLuong)}</h3>`;
}
/**
 * bài 2
 * input:
 * nhập 5 số từ bàn phím
 * 
 * bước xử lý
 * b1: tạo mảng numbers, biến sum
 * b2: dùng vòng lặp for để duyệt mảng công dồn các phần tử lại
 * b3: tính tb = tổng / độ dài của mảng (sum/numbers.length)
 * b4: in kết quả tb bằng innerHTML
 * 
 * output:
 *  kết quả tb (Trung Bình)
 */
function tinhTB(){
    var numbers =  document.getElementsByClassName("soNhapVao");
    var sum = 0;
    for(var i = 0; i < numbers.length; i++) {
        sum += numbers[i].value*1;
    }
    result.innerHTML = `<h3>${sum/numbers.length}</h3>`;
}

/**
 * bài 3
 * input:
 *  1 USD = 23.500 VND
 *  nhập số USD từ bàn phím
 * 
 * bước xử lý
 * b1: tạo biến soUSD, sumVND;
 * b2: lấy giá trị soUSD bằng value
 * b3: tính sumVND = soUSD * 235000;
 * b4: in kết quả sumVND bằng innerHTML
 * 
 * output:
 *  kết quả sumVND (số tiền VND)
 */
function doiTien() {
    var soUSD = document.getElementById("soUSD").value*1;
    var sumVND = 23500 * soUSD;
    result.innerHTML = `<h3>${new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(sumVND)}</h3>`;
}

/**
 * bài 4
 * input:
 *  nhập cạnh dài từ bàn phím
 *  nhập cạnh rộng từ bàn phím
 *  
 * 
 * bước xử lý
 * b1: tạo biến chieuDai, chieuRong, dienTich, chuVi
 * b2: lấy giá trị chieuDai, chieuRong bằng value
 * b3: tính chuvi = (dai + rong) * 2
 * b4: tính dientich = dai * rong
 * b5: in kết quả dientich, chuvi bằng innerHTML
 * 
 * output:
 *  kết quả chuvi, dientich (chu vi, diện tích)
 */
//bai4
function tinhHCN(){
    var chieuDai = document.getElementById("chieuDai").value*1;
    var chieuRong = document.getElementById("chieuRong").value*1;
    var dienTich = chieuDai * chieuRong;
    var chuVi = (chieuDai + chieuRong)*2;
    result.innerHTML = `<h3>Diện tich: ${dienTich}; Chu vi: ${chuVi}</h3>`;
}

/**
 * bài 5
 * input:
 *  nhập số có 2 chữ số từ bàn phím
 * 
 * bước xử lý
 * b1: tạo biến kySo, hangchuc, donvi, sumKySo;
 * b2: lấy giá trị kySo bằng value
 * b3: tách số hàng chục = Math.floor(kySo/10)
 * b4: in kết quả sumKySo ra bằng innerHTML
 * 
 * output:
 *  kết quả tổng 2 ký số
 */
function tongKySo(){
    var kySo = document.getElementById("kySo").value*1;
    var hangChuc, donVi, sumKySo;
    hangChuc = Math.floor(kySo/10);
    donVi = kySo % 10;
    sumKySo = hangChuc + donVi;
    result.innerHTML = `<h3>${sumKySo}</3>`;
}



